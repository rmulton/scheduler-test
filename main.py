import datetime
import requests
if __name__=="__main__":
    now = datetime.datetime.now()
    print("Scheduler ran at {}".format(now))
    r = requests.post('https://webapp-test-scheduler.herokuapp.com/api/Records', data = {'time': now})
    print("Request : {}".format(r)) 
